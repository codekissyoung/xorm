package main

import (
	"fmt"
	"xorm.io/xorm"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	"time"
	"xorm.io/builder"
)

type User struct {
	Id int64
	Name string
	Salt string
	Age int
	Passwd string `xorm:"varchar(200)"`
	Created time.Time `xorm:"created"`
	Updated time.Time `xorm:"updated"`
}

func main(){
	dsn := "root:Pwd123456@tcp(127.0.0.1:3306)/go_model_test?charset=utf8mb4"
	e, err := xorm.NewEngine("mysql", dsn)
	if err != nil {
		fmt.Println(err)
	}

	// err = e.Sync2(&User{})
	// if err != nil {
	// 	fmt.Println(err)
	// }

	s := e.NewSession()
	err = s.Ping()
	if err != nil {
		fmt.Println(err)
	}

	insertNum, err := s.Insert(&User{
		Name : "link",
		Salt: "aaaaa",
		Age:11,
		Passwd: "ssfdfaef",
	})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("insertNum : %d \n", insertNum)

	sql, args := s.LastSQL()
	sql, _ = builder.ConvertToBoundSQL(sql, args)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("%v \n", sql)




}
